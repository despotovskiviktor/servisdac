<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        if (\Auth::user()->role->name === 'Administrator')
        {
            return $next($request);
        }
        return redirect()->route('permissions');

    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Helper\ImageStore;
use App\Models\Categories;
use App\Models\Countries;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class ProductsController extends Controller
{

    public function index()
    {
        $products = Products::all();
        $data = ['products' => $products];
        return view('products.index')->with($data);
    }

    public function create()
    {
        $categories = Categories::getList();
        $countries = Countries::all();
        $data = ['countries' => $countries, 'categories' => $categories];
        return view('products.create')->with($data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'quantity'  => ['required', 'integer',],
            'price'      => ['required', 'integer'],
            'image'   => ['required', 'image'],
            'cat_id'       => ['required', 'integer'],
            'country_id'       => ['required', 'integer']
        ]);

        if ($validator->fails()) {
            return redirect('admin/products/create')
                ->withErrors($validator)
                ->withInput();
        }
        $imageObj = new ImageStore($request, 'products');
        $image = $imageObj->imageStore();

        Products::create([
            'name' => $request->get('name'),
            'quantity'  => $request->get('quantity'),
            'price'      => $request->get('price'),
            'image'      => $image,
            'cat_id'    => $request->get('cat_id'),
            'country_id'    => $request->get('country_id')

        ]);

        Session::flash('flash_message', 'Product successfully created!');
        $products = Products::all();
        $data = ['products' => $products];
        return redirect('/admin/products')->with($data);
    }

    public function show($id)
    {
        $product = Products::FindOrFail($id);
        $country = Countries::all();
        $category = Categories::all();
        $data = ["product" => $product, "country" => $country, "category" => $category];
        return view('products.show')->with($data);
    }

    public function edit($id)
    {
        $product = Products::FindOrFail($id);
        $country = Countries::all();
        $category = Categories::all();
        $data = ["product" => $product, "country" => $country, "category" => $category];
        return view('users.edit')->with($data);
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}

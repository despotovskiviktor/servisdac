<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Helper\ImageStore;


class UserController extends Controller
{

    public function index()
    {
        $users = User::paginate(5);
        $data = ["users" => $users];
        return view('users.index')->with($data);
    }

    public function create()
    {
        $roles = Roles::all();
        $data = ["roles" => $roles];
        return view('users.create')->with($data);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
                'role_id' => ['required', 'integer']
        ]);

        if ($validator->fails()) {
            return redirect('user/create')
                ->withErrors($validator)
                ->withInput();
        }

        $imageObj = new ImageStore($request, 'users');
        $image = $imageObj->imageStore();

        User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'image' => $image,
            'role_id' => $request->get('role_id'),
            'password' => Hash::make($request->get('password')),

        ]);

        Session::flash('flash_message', 'User successfully created!');
        $users = User::paginate(5);
        $data = ["users" => $users];
        return redirect('/admin/users')->with($data);

    }

    public function show($id)
    {
        $user = User::FindOrFail($id);
        $roles = Roles::all();
        $data = ["user" => $user, "roles" => $roles];
        return view('users.show')->with($data);
    }

    public function edit($id)
    {
        $user = User::FindOrFail($id);
        $roles = Roles::all();
        $data = ["user" => $user, "roles" => $roles];
        return view('users.edit')->with($data);
    }

    public function update(Request $request, User $user)
    {
        if ($request->has('password') && !empty($request->get('password'))) {
            $input['password'] = Hash::make($request->get('password'));
        } else {
            $input = $request->except(['password']);
        }

        if ($request->hasFile('image')) {
            $imageObj = new ImageStore($request, 'users');
            $image = $imageObj->imageStore();
            $input['image'] = $image;
        }

        $user->fill($input)->save();

        $users = User::paginate(5);
        $data = ['users' => $users];
        return view('users.index')->with($data);
    }

    public function destroy($id)
    {
        $user = User::FindOrFail($id);
        $user->delete();
        $users = User::paginate(5);
        $data = ["users" => $users];
        return view('users.index')->with($data);

    }
}

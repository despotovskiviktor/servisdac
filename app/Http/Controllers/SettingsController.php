<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Helper\ImageStore;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Settings::all();
        $data = ['settings' => $settings];
        return view('settings.index')->with($data);
    }

    public function create()
    {
        return view('settings.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => ['required', 'string', 'max:255'],
            'facebook' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'linked_in' => ['required', 'string', 'min:8'],
            'twitter' => ['required', 'string', 'min:8'],
            'instagram' => ['required', 'string', 'min:8'],
            'description' => ['required', 'string', 'min:8'],
            'phone' => ['required', 'string', 'min:8'],
            'image' => ['required', 'image'],
            'title' => ['required', 'string', 'min:8']
        ]);

        if ($validator->fails()) {

            return redirect('admin/settings/create')
                ->withErrors($validator)
                ->withInput();
        }

        $imageObj = new ImageStore($request, 'settings');
        $image = $imageObj->imageStore();

        Settings::create([
            'url' => $request->get('url'),
            'facebook' => $request->get('facebook'),
            'instagram' => $request->get('instagram'),
            'email' => $request->get('email'),
            'logo' => $image,
            'twitter' => $request->get('twitter'),
            'linked_in' => $request->get('linked_in'),
            'description' => $request->get('description'),
            'phone' => $request->get('phone'),
            'title' => $request->get('title')
        ]);

        Session::flash('flash_message', 'Settings successfully created!');
        return redirect()->back();
    }

    public function show($id)
    {
        //TODO
    }
    public function edit($id)
    {
        //TODO
    }
    public function update(Request $request, $id)
    {
        //TODO
    }

    public function destroy($id)
    {
        //TODO
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'name',
        'quantity',
        'price',
        'image',
        'country_id',
        'cat_id'
    ];

    public function categories()
    {
        return $this->belongsTo(Categories::class);
    }

    public function countries()
    {
        return $this->belongsTo(Countries::class, 'country_id');
    }
}



<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleMakes extends Model
{
    use HasFactory;
    protected $table = 'vehicle_makes';

    public function vehicle_models()
    {
        return $this->belongsTo(VehicleModels::class, 'make_id');
    }
}

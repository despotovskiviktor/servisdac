<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => ['web', 'auth', 'admin'],'prefix' => 'admin'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('companies', App\Http\Controllers\CompaniesController::class);
    Route::resource('categories', App\Http\Controllers\CategoriesController::class);
    Route::get('categories/{id}/delete', [App\Http\Controllers\CategoriesController::class, 'destroy']);
    Route::resource('products', App\Http\Controllers\ProductsController::class);
    Route::resource('settings', App\Http\Controllers\SettingsController::class);
});
Route::get('permissions', [App\Http\Controllers\HomeController::class, 'permissions'])->name('permissions');


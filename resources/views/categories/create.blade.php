@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row">

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif


            <div class="col-12">
                <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf


                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Category Name</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" autocomplete="name" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="parent_id" class="col-md-4 col-form-label text-md-right">Subcategory</label>
                        <div class="col-md-6">
                            <select class="form-control @error('parent_id') is-invalid @enderror" name="parent_id"
                            >
                                <option value="">Main Category</option>
                                {!! $categories !!}
                            </select>
                            @error('parent_id')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection

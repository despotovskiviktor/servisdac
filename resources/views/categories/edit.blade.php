@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('categories.update', $category) }}" method="POST">
                    @method('PUT')
                    @csrf

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Category Name</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ $category->name }}" autocomplete="name" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="parent_id" class="col-md-4 col-form-label text-md-right">Subcategory</label>
                        <div class="col-md-6">
                            <select class="form-control @error('parent_id') is-invalid @enderror" name="parent_id"
                            >
                                <option value="">Main Category</option>
                                {!! $categories !!}
                            </select>
                            @error('parent_id')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <form action="{{ route('categories.destroy', $category) }}" method="POST">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>

@endsection

@section('scripts')
    <script>
        $('[name=parent_id]').val({{ $category->parent_id }});
    </script>
@endsection

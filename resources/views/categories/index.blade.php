@extends('layouts.dashboard')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-8">
                <a href="{{ route('categories.create') }}" class="btn btn-primary">Add Category</a>
            </div>
        </div>
        {!! $categories  !!}
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('user.update', $user) }}" method="POST" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                    @csrf

                    <div class="form-group row">

                        <label for="image" class="col-md-4 col-form-label text-md-right">Upload image</label>
                        <div class="col-md-6">
                            <input id="image" type="file" class="btn btn-warning" name="image">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">First Name</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="first_name" required autocomplete="name" autofocus value="{{ $user->first_name }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Last Name</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="last_name" required value="{{ $user->last_name }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right" >Email</label>
                        <div class="col-md-6">
                            <input id="name" type="email" class="form-control" name="email" required value="{{ $user->email }}">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Password</label>
                        <div class="col-md-6">
                            <input id="name" type="password" class="form-control" name="password">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                        <div class="col-md-6">
                            <input id="name" type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                        <div class="col-md-6">
                            <select class="form-control" name="role_id" required>
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}" @if($user->role_id == $role->id)selected @endif>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-info" >Submit</button>
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>
@endsection

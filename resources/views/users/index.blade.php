@extends('layouts.dashboard')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-green">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col"></th>
                        <th scope="col">Full Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Role</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td><a href="/admin/user/{{ $user->id }}">{{ $user->id }}</a></td>
                        <td><img class="img-wrapper" width="40px" height="40px" src="/assets/img/users/thumbnails/{{ $user->image }}"></td>
                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role->name}}</td>
                    </tr>
                    </tbody>
                    @endforeach
                </table>
                {{ $users->links() }}
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <a class="btn btn-outline-success" href="/user/create">+ Create User</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.dashboard')


@section('content')
    <div class="container">
        <div class="row">

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif

            <div class="col-12">
                <form action="{{ route('settings.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label class="btn btn-default">
                                    Upload Logo <input type="file" id="image" name="image" hidden>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="url" class="col-md-4 col-form-label text-md-right">URL</label>
                        <div class="col-md-6">
                            <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" value="{{ old('url') }}" name="url" autocomplete="url" required autofocus>
                            @error('url')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror

                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>
                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}" name="title" required >
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Email</label>
                        <div class="col-md-6">
                            <input id="name" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="facebook" class="col-md-4 col-form-label text-md-right">Facebook</label>
                        <div class="col-md-6">
                            <input id="facebook" type="text" class="form-control @error('facebook') is-invalid @enderror" name="facebook" required>
                            @error('facebook')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="linked_in" class="col-md-4 col-form-label text-md-right">LinkedIn</label>
                        <div class="col-md-6">
                            <input id="linked_in" type="text" class="form-control @error('linked_in') is-invalid @enderror" name="linked_in" required>
                            @error('linked_in')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="twitter" class="col-md-4 col-form-label text-md-right">Twitter</label>
                        <div class="col-md-6">
                            <input id="twitter" type="text" class="form-control @error('twitter') is-invalid @enderror" name="twitter" required>
                            @error('twitter')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="instagram" class="col-md-4 col-form-label text-md-right">Instagram</label>
                        <div class="col-md-6">
                            <input id="instagram" type="text" class="form-control @error('instagram') is-invalid @enderror" name="instagram" required>
                            @error('instagram')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label text-md-right">Phone</label>
                        <div class="col-md-6">
                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" required>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                        <div class="col-md-6">
                            <textarea id="description" type="text" class="form-control" name="description" required></textarea>
                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-info">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
<script>
    CKEDITOR.replace( 'description' );
</script>
@endsection

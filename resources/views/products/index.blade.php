@extends('layouts.dashboard')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-green">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col"></th>
                        <th scope="col">Name</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                        <th scope="col">Country</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td><a href="/admin/user/{{ $product->id }}">{{ $product->id }}</a></td>
                            <td><img class="img-wrapper" width="40px" height="40px" src="/assets/img/products/thumbnails/{{ $product->image }}"></td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->quantity}}</td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->countries->full_name}}</td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <a class="btn btn-outline-success" href="/admin/products/create">+ Add Product</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

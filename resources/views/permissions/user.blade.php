@extends('layouts.dashboard')
@section('content')
<div class="card text-center">
    <div class="card-body">
        <div class="card-header">
            <h1>Access Denied</h1>
        </div>
        <h2 class="card-title">{{ \Auth::user()->first_name }} , You dont have permission to access this site!!</h2>
        <a class="btn btn-primary" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </div>

</div>
@endsection